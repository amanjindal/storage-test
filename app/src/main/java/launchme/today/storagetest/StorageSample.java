package launchme.today.storagetest;

/**
 * Created by pooja on 10/27/15.
 */

import android.app.Activity;
import android.util.Log;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.SecurityUtils;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.ObjectAccessControl;
import com.google.api.services.storage.model.Objects;
import com.google.api.services.storage.model.StorageObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Main class for the Cloud Storage JSON API sample.
 *
 * Demonstrates how to make an authenticated API call using the Google Cloud Storage API client
 * library for java, with Application Default Credentials.
 */
public class StorageSample {

    public static void setActivity(Activity activity) {
        mActivity = activity;
    }

    static Activity mActivity;
    /**
     * Be sure to specify the name of your application. If the application name is {@code null} or
     * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
     */
    private static final String APPLICATION_NAME = "[[INSERT_YOUR_APP_NAME_HERE]]";

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TEST_FILENAME = "json-test.txt";

    // [START get_service]
    private static Storage storageService;

    /**
     * Returns an authenticated Storage object used to make service calls to Cloud Storage.
     */
    private static Storage getService() throws IOException, GeneralSecurityException {

        if(storageService != null)
            return storageService;

        HttpTransport httpTransport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        List<String> scopes = new ArrayList<String>();
        scopes.add(StorageScopes.DEVSTORAGE_FULL_CONTROL);
        String tmpfile = mActivity.getExternalCacheDir() + "/" + "test.pem" ;


        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            // read this file into InputStream
            inputStream = mActivity.getResources().openRawResource(R.raw.unprotected_p12);

            Log.e("HAHA", tmpfile + "");
            // write the inputStream to a FileOutputStream
            outputStream =
                    new FileOutputStream(tmpfile);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            System.out.println("Done!");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

                /*KeyStore keystore = KeyStore.getInstance("PKCS12");
                String p12Password="notasecret";
                keystore.load(mActivity.getResources().openRawResource(R.raw.spiderme_p12), p12Password.toCharArray());
                key = (PrivateKey)keystore.getKey("google", p12Password.toCharArray());
                */
        PrivateKey key;

        key = SecurityUtils.loadPrivateKeyFromKeyStore(
                SecurityUtils.getPkcs12KeyStore(), mActivity.getResources().openRawResource(R.raw.spiderme_p12), "notasecret",
                "privatekey", "notasecret");



        Credential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId("1019837521318-88ojbeu6t21juaa6p0hff9liu06jjvnq@developer.gserviceaccount.com") //Email
                        //.setServiceAccountPrivateKeyFromP12File(new File(tmpfile))
                .setServiceAccountPrivateKey(key)
                .setServiceAccountScopes(scopes).build();
        storageService = new Storage.Builder(httpTransport, jsonFactory,
                credential).setApplicationName("MyCompany-ProductName/1.0")
                .build();
        return storageService;
    }
    // [END get_service]

        // [START list_bucket]
        /**
         * Fetch a list of the objects within the given bucket.
         *
         * @param bucketName the name of the bucket to list.
         * @return a list of the contents of the specified bucket.
         */
    public static List<StorageObject> listBucket(String bucketName)
            throws IOException, GeneralSecurityException {
        Storage client = getService();
        Storage.Objects.List listRequest = client.objects().list(bucketName);

        List<StorageObject> results = new ArrayList<StorageObject>();
        Objects objects;

        // Iterate through each page of results, and add them to our results list.
        do {
            objects = listRequest.execute();
            // Add the items in this page of results to the list we'll return.
            results.addAll(objects.getItems());

            // Get the next page, in the next iteration of this loop.
            listRequest.setPageToken(objects.getNextPageToken());
        } while (null != objects.getNextPageToken());

        return results;
    }
    // [END list_bucket]

    // [START get_bucket]
    /**
     * Fetches the metadata for the given bucket.
     *
     * @param bucketName the name of the bucket to get metadata about.
     * @return a Bucket containing the bucket's metadata.
     */
    public static Bucket getBucket(String bucketName) throws IOException, GeneralSecurityException {
        Storage client = getService();

        Storage.Buckets.Get bucketRequest = client.buckets().get(bucketName);
        // Fetch the full set of the bucket's properties (e.g. include the ACLs in the response)
        bucketRequest.setProjection("full");
        return bucketRequest.execute();
    }
    // [END get_bucket]

    // [START upload_stream]
    /**
     * Uploads data to an object in a bucket.
     *
     * @param name the name of the destination object.
     * @param contentType the MIME type of the data.
     * @param stream the data - for instance, you can use a FileInputStream to upload a file.
     * @param bucketName the name of the bucket to create the object in.
     */
    public static void uploadStream(
            String name, String contentType, InputStream stream, String bucketName)
            throws IOException, GeneralSecurityException {
        InputStreamContent contentStream = new InputStreamContent(contentType, stream);
        StorageObject objectMetadata = new StorageObject()
                // Set the destination object name
                .setName(name)
                        // Set the access control list to publicly read-only
                .setAcl(Arrays.asList(
                        new ObjectAccessControl().setEntity("allUsers").setRole("READER")));

        // Do the insert
        Storage client = getService();
        Storage.Objects.Insert insertRequest = client.objects().insert(
                bucketName, objectMetadata, contentStream);

        insertRequest.execute();
    }
    // [END upload_stream]

    // [START delete_object]
    /**
     * Deletes an object in a bucket.
     *
     * @param path the path to the object to delete.
     * @param bucketName the bucket the object is contained in.
     */
    public static void deleteObject(String path, String bucketName)
            throws IOException, GeneralSecurityException {
        Storage client = getService();
        client.objects().delete(bucketName, path).execute();
    }
    // [END delete_object]

    /**
     * Exercises the class's functions - gets and lists a bucket, uploads and deletes an object.
     *
     * @param args the command-line arguments. The first argument should be the bucket name.
     */
    public static void main2(String args) {

        String bucketName = args;

        try {
            // Get metadata about the specified bucket.
            Bucket bucket = getBucket(bucketName);
            System.out.println("name: " + bucketName);
            System.out.println("location: " + bucket.getLocation());
            System.out.println("timeCreated: " + bucket.getTimeCreated());
            System.out.println("owner: " + bucket.getOwner());


            // List the contents of the bucket.
            List<StorageObject> bucketContents = listBucket(bucketName);
            if (null == bucketContents) {
                System.out.println(
                        "There were no objects in the given bucket; try adding some and re-running.");
            }
            for (StorageObject object : bucketContents) {
                System.out.println(object.getName() + " (" + object.getSize() + " bytes)");
            }


            // Upload a stream to the bucket. This could very well be a file.
            uploadStream(
                    "haha.txt", "text/plain",
                    mActivity.getResources().openRawResource(R.raw.sample_png),
                    bucketName);

            // Now delete the file
            deleteObject(TEST_FILENAME, bucketName);

        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }
}
